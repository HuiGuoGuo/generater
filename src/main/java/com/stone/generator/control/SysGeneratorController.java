package com.stone.generator.control;

import com.stone.generator.pojo.Query;
import com.stone.generator.pojo.ResultBean;
import com.stone.generator.pojo.request.GeneratorProjectRequestDTO;
import com.stone.generator.pojo.request.GeneratorRequestDTO;
import com.stone.generator.pojo.request.ListTableRequestDTO;
import com.stone.generator.pojo.vo.PageTableVO;
import com.stone.generator.pojo.vo.TableVO;
import com.stone.generator.service.SysGeneratorService;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.List;

/**
 * Created by Stone on 2018/7/27.
 */

@RestController
@RequestMapping("/sys/generator")
public class SysGeneratorController {

    @Resource
    private SysGeneratorService sysGeneratorService;

    @GetMapping(value = "/listTable")
    public ResultBean<PageTableVO> listTable(ListTableRequestDTO requestDTO) {
        Query query = new Query(requestDTO);
        ResultBean<List<TableVO>> listResultBean = sysGeneratorService.listTable(query);
        ResultBean<Integer> countTable = sysGeneratorService.countTable(query);
        PageTableVO pageTableVO = new PageTableVO(query.getPage(), query.getPageSize(), countTable.getData(), listResultBean.getData());
        ResultBean<PageTableVO> resultBean = ResultBean.create();
        return resultBean.setData(pageTableVO);
    }

    @SneakyThrows
    @GetMapping(value = "/{tableName}")
    public void download(@PathVariable("tableName") String tableName, GeneratorRequestDTO requestDTO, HttpServletResponse response) {
        Query query = new Query();
        query.put("tableName", tableName);
        query.put("groupId", requestDTO.getPackageName());
        query.put("artifactId", requestDTO.getModuleName());
        byte[] data = sysGeneratorService.download(query);
        @Cleanup OutputStream outputStream = response.getOutputStream();
        response.reset();
        response.setHeader("Content-Disposition", String.format("attachment; filename=%s.zip", tableName));
        response.addHeader("Content-Length", String.format("%s", data.length));
        response.setContentType("application/octet-stream; charset=UTF-8");
        IOUtils.write(data, outputStream);
    }

    @SneakyThrows
    @GetMapping(value = "/project")
    public void downloadProject(GeneratorProjectRequestDTO requestDTO, HttpServletResponse response) {
        byte[] data = sysGeneratorService.downloadProject(requestDTO);
        @Cleanup OutputStream outputStream = response.getOutputStream();
        response.reset();
        response.setHeader("Content-Disposition", String.format("attachment; filename=%s.zip", requestDTO.getModuleName()));
        response.addHeader("Content-Length", String.format("%s", data.length));
        response.setContentType("application/octet-stream; charset=UTF-8");
        IOUtils.write(data, outputStream);
    }
}
