package com.stone.generator.pojo.info;

import com.stone.generator.pojo.request.GeneratorProjectRequestDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author : WH
 * @date : 2018/11/19 10:07 AM
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ProjectInfo extends BaseInfo {

    private static final long serialVersionUID = -5837536938665927651L;

    private String version;

    private String moduleName;

    private String moduleNameUp;

    private String moduleNameLow;

    private String modulePrefix;

    private String packagePrefix;

    private String moduleUp ;

    private List<String> profiles;

    private GeneratorProjectRequestDTO.DataSource dataSource;
}
