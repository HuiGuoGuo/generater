FROM openjdk:8-jdk-alpine
MAINTAINER wanghui9071@vantai.com
VOLUME /tmp
COPY generator-3.0.jar generator.jar
ENTRYPOINT ["java","-jar","/generator.jar"]