function generatorProject() {
    var groupId = $('input[name=groupId]').val();
    var artifactId = $('input[name=artifactId]').val();
    var version = $('input[name=version]').val();
    var moduleName = $('input[name=moduleName]').val();
    var host = $('input[name=host]').val();
    var port = $('input[name=port]').val();
    var username = $('input[name=username]').val();
    var password = $('input[name=password]').val();
    var dbName = $('input[name=dbName]').val();
    var driverClass = $('input[name=driverClass]').val();
    window.location.href = 'sys/generator/project?groupId=' + groupId + '&artifactId=' + artifactId + '&version=' + version + '&moduleName=' + moduleName
        + '&dataSource.host=' + host + "&dataSource.port=" + port + "&dataSource.username=" + username + "&dataSource.password=" + password
        + "&dataSource.name=" + dbName + "&dataSource.driverClass=" + driverClass;
}

$("input[name='artifactId']").on('change', function () {
    console.log($(this).val());
    $("input[name='moduleName']").val($(this).val());
});


