package com.stone.generator.pojo.request;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * author : WH
 * time : 2018/11/19 3:55 PM
 */

@Data
@Accessors(chain = true)
public class GeneratorProjectRequestDTO implements Serializable {

    private static final long serialVersionUID = -739017512750057981L;

    private String groupId;

    private String artifactId;

    private String version;

    private String moduleName;

    private DataSource dataSource;

    @Data
    public static class DataSource implements Serializable {

        private static final long serialVersionUID = -3308073589105894398L;

        private String host;

        private Integer port = 3306;

        private String name;

        private String username;

        private String password;

        private String driverClass = "com.mysql.cj.jdbc.Driver";
    }
}
