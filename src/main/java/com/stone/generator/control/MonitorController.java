package com.stone.generator.control;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author : WangHui
 * @date :   2021/4/8
 */
@Api(value = "Monitor")
@RestController
public class MonitorController {

    @GetMapping(value = "/monitor")
    public String monitor(){
        return "200";
    }
}
